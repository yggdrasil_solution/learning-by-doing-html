Folder PATH listing for volume YosuaExt7Videos1
Volume serial number is C1D6-12D0
G:.
+---[Animation]
|   +---[DC]
|   |   \---Isekai Suicide Squad (2024)
|   +---[Disney]
|   |   +---A Bug's Life (1998)
|   |   +---Atlantis (2001,2003)
|   |   +---Baymax! (2022) [Series]
|   |   +---Big Hero 6 (2014)
|   |   +---Bolt (2008)
|   |   +---Brave (2012)
|   |   +---Cars (2006,2011,2017)
|   |   +---Cars on the Road (2022) [Series]
|   |   +---Chicken Little (2005)
|   |   +---Elemental (2023)
|   |   +---Encanto (2021)
|   |   +---Enchanted (2007,2022)
|   |   +---Finding Nemo (2003,2016)
|   |   +---Frozen (2013,2019)
|   |   +---Good Dinosaur, The (2015)
|   |   +---Incredibles, The (2004,2018)
|   |   +---Inside Out (2015,2024)
|   |   +---Lightyear (2022)
|   |   +---Lilo & Stitch (2002,2005)
|   |   +---Luca (2021)
|   |   +---Mickey and the Roadster Racers (2017,2018.2019)
|   |   |   +---Mickey and the Roadster Racers S1
|   |   |   +---Mickey and the Roadster Racers S2
|   |   |   \---Mickey and the Roadster Racers S3
|   |   +---Mickey Mouse (1999,2004)
|   |   +---Moana (2016,2024)
|   |   +---Mulan (1998,2004)
|   |   +---Onward (2020)
|   |   +---Planes (2013,2014)
|   |   +---Ralph (2012,2018)
|   |   +---Ratatouille (2007)
|   |   +---Raya and the Last Dragon (2021)
|   |   +---Ron's Gone Wrong (2021)
|   |   +---Soul (2020)
|   |   +---Spies in Disguise (2019)
|   |   +---Strange World (2022)
|   |   +---Tangled (2010)
|   |   +---Tinker Bell (2008,2009,2010,2012,2014,2015)
|   |   +---Toy Story (1995,1999,2010,2019)
|   |   +---Treasure Planet (2002)
|   |   +---Turning Red (2022)
|   |   +---Up (2009)
|   |   +---WALL-E (2008)
|   |   \---Wish (2023)
|   +---[Lego]
|   |   +---Lego Batman (2013,2017,2019)
|   |   +---Lego DC Comics Super Heroes (2014,2015,2015,2016,2016,2018,2018,2020)
|   |   +---Lego DC Super Hero Girls (2017,2018)
|   |   +---Lego Jurassic World (2016,2019)
|   |   +---Lego Marvel (2019,2020)
|   |   +---Lego Marvel Super Heroes (2013,2015,2017,2018)
|   |   +---Lego Movie, The (2014,2019)
|   |   +---Lego Ninjago Movie, The (2017)
|   |   \---Lego Scooby-Doo! (2015,2016,2017)
|   +---[Marvel]
|   |   \---Spider-Man - Spider-Verse (2018,2023)
|   \---[Others]
|       +---100% Wolf (2020,2024)
|       +---Adventures of Tintin, The (2011)
|       +---Asterix (2014,2018)
|       +---Astro Boy (2009)
|       +---Avatar
|       |   +---Avatar - The Last Airbender (2005,2006,2007) [Series]
|       |   |   +---Bending Skill Trees
|       |   |   +---Creating The Legend
|       |   |   \---Super Deformed Shorts
|       |   +---Avatar - The Last Airbender (2010) [Movie]
|       |   +---Avatar - The Last Airbender (2024) [Series]
|       |   \---Avatar - The Legend of Korra (2012,2013,2014,2014) [Series]
|       |       \---Bending Skill Trees
|       +---BoBoiBoy Galaxy (2016) [Series]
|       +---Boss Baby, The (2017,2021)
|       +---Fearless (2020)
|       +---Fireheart (2022)
|       +---Gnome Alone (2017)
|       +---Gnomeo & Juliet (2011,2018)
|       +---Ice Age (2002,2006,2009,2012,2016)
|       +---Mitchells vs the Machines, The (2021)
|       +---Monkey King Reborn (2021)
|       +---Monkey King, The (2023)
|       +---Next Gen (2018)
|       +---Samsam (2019)
|       +---Scoob! (2020)
|       +---Teenage Mutant Ninja Turtles - Mutant Mayhem (2023)
|       +---TMNT (2007)
|       +---Transformers One (2024)
|       +---Turbo (2013)
|       \---Wish Dragon (2021)
+---[Game Adaptation]
|   +---Ange Vierge (2016) [Anime]
|   +---Angry Birds Movie, The (2016,2019) [Animation]
|   +---Arknights (2022,2023) [Anime]
|   +---Assassin's Creed (2016) [Movie]
|   +---Azur Lane (2019) [Anime]
|   +---Borderlands (2024) [Movie]
|   +---DOA - Dead Or Alive (2006) [Movie]
|   +---Dolls' Frontline (2022) [Anime]
|   +---Dragon Nest (2014,2017) [Animation]
|   +---Dragon Quest - Your Story (2019) [Animation]
|   +---Dungeons & Dragons - Honor Among Thieves (2023) [Movie]
|   +---Final Fantasy [Animation�Anime]
|   |   +---Final Fantasy - Legend of the Crystals (1994) [Anime]
|   |   +---Final Fantasy - The Spirits Within (2001) [Animation]
|   |   +---Final Fantasy - Unlimited (2001) [Anime]
|   |   +---Final Fantasy VII - Advent Children (2005) [Animation]
|   |   +---Final Fantasy VII - Last Order (2005) [Anime]
|   |   +---Final Fantasy VII - On the Way to a Smile (2009) [Anime]
|   |   +---Final Fantasy XV - Brotherhood (2016) [Anime]
|   |   +---Final Fantasy XV - Episode Ardyn - Prologue (2019) [Anime]
|   |   \---Final Fantasy XV - Kingsglaive (2016) [Animation]
|   +---Galaxy Fraulein Yuna (1995) [Anime]
|   +---Gran Turismo (2023) [Movie]
|   +---Grimms Notes - The Animation (2019) [Anime]
|   +---Hitman (2007,2015) [Movie]
|   +---Hyperdimension Neptunia - The Animation (2013) [Anime]
|   +---King of Fighters, The (2010) [Movie]
|   +---Lara Croft - Tomb Raider (2001,2003) [Movie]
|   +---Monster Hunter [Animation�Movie]
|   |   +---Monster Hunter (2020) [Movie]
|   |   \---Monster Hunter - Legends of the Guild (2021) [Animation]
|   +---Mortal Kombat (1995,1997) [Movie]
|   +---Mortal Kombat (2021) [Movie]
|   +---Need for Speed (2014) [Movie]
|   +---NieR - Automata Ver1.1a (2023) [Anime]
|   +---Pokemon [Animation�Movie]
|   |   +---Pokemon - Detective Pikachu (2019) [Movie]
|   |   \---Pokemon - Mewtwo Strikes Back - Evolution (2019) [Animation]
|   +---Prince of Persia - The Sands of Time (2010) [Movie]
|   +---Rampage (2018) [Movie]
|   +---Ratchet & Clank (2016) [Animation]
|   +---Resident Evil [Animation�Movie�Series]
|   |   +---Resident Evil (2002,2004,2007,2010,2012,2016) [Movie]
|   |   +---Resident Evil (2008,2012,2017,2021,2023) [Animation]
|   |   +---Resident Evil (2021) [Movie]
|   |   \---Resident Evil (2022) [Series]
|   +---Schoolgirl Strikers - Animation Channel (2017) [Anime]
|   +---Silent Hill (2006,2012) [Movie]
|   +---Sonic the Hedgehog [Animation�Movie�Series]
|   |   +---Knuckles (2024) [Series]
|   |   +---Sonic Prime (2022) [Animation]
|   |   \---Sonic the Hedgehog (2020,2022,2024) [Movie]
|   +---Street Fighter [Movie]
|   |   +---Street Fighter (1994) [Movie]
|   |   \---Street Fighter (2014) [Movie]
|   +---Super Mario Bros. Movie, The (2023) [Animation]
|   +---Tekken [Animation�Movie]
|   |   +---Tekken (2009,2014) [Movie]
|   |   \---Tekken (2011) [Animation]
|   +---Tomb Raider (2018) [Movie]
|   +---Tron - Legacy (2010) [Movie]
|   +---Twisted Metal (2023) [Series]
|   +---Uncharted (2022) [Movie]
|   \---Warcraft (2016) [Movie]
+---[Live Action]
|   +---[DC]
|   |   +---Constantine (2005)
|   |   +---Green Lantern (2011)
|   |   +---RED (2010,2013)
|   |   +---V for Vendetta (2005)
|   |   +---Watchmen (2009)
|   |   \---[DC Extended Universe]
|   |       +---Aquaman (2018,2023)
|   |       +---Batman v Superman - Dawn of Justice (2016)
|   |       +---Birds of Prey (2020)
|   |       +---Black Adam (2022)
|   |       +---Blue Beetle (2023)
|   |       +---Flash, The (2023)
|   |       +---Justice League (2017)
|   |       +---Man of Steel (2013)
|   |       +---Peacemaker (2022) [Series]
|   |       +---Shazam! (2019,2023)
|   |       +---Suicide Squad (2016,2021)
|   |       +---Wonder Woman (2017,2020)
|   |       \---Zack Snyder's Justice League (2021)
|   +---[Disney]
|   |   +---Aladdin (2019)
|   |   +---Alice (2010,2016)
|   |   +---Beauty and the Beast (2017)
|   |   +---Chip 'n Dale - Rescue Rangers (2022)
|   |   +---Christopher Robin (2018)
|   |   +---Cinderella (2015)
|   |   +---Cruella (2021)
|   |   +---Dalmatians (1996,2000)
|   |   +---Dumbo (2019)
|   |   +---Jungle Book, The (2016)
|   |   +---Lion King, The (2019,2024)
|   |   +---Little Mermaid, The (2023)
|   |   +---Maleficent (2014,2019)
|   |   +---Mulan (2020)
|   |   +---Peter Pan & Wendy (2023)
|   |   \---Pinocchio (2022)
|   +---[Marvel]
|   |   +---Amazing Spider-Man, The (2012,2014)
|   |   +---Blade (1998,2002,2004)
|   |   +---Daredevil (2003,2005)
|   |   +---Fantastic Four (2005,2007,2015)
|   |   +---Ghost Rider (2007,2011)
|   |   +---Gifted, The (2017,2018) [Series]
|   |   +---Kingsman (2014,2017,2021)
|   |   +---Legion (2017,2018,2019) [Series]
|   |   +---Men in Black (1997,2002,2012,2019)
|   |   +---Punisher (2004,2008)
|   |   +---Spider-Man (2002,2004,2007)
|   |   +---X-Men [Film Series]
|   |   |   +---Deadpool (2016,2018)
|   |   |   +---New Mutants, The (2020)
|   |   |   +---Wolverine (2009,2013,2017)
|   |   |   +---X-Men (2000,2003,2006)
|   |   |   \---X-Men (2011,2014,2016,2019)
|   |   +---[Marvel Cinematic Universe]
|   |   |   +---Agents of S.H.I.E.L.D (2013,2014,2015,2016,2017,2019,2020) [Series]
|   |   |   |   +---Agents of S.H.I.E.L.D. S1
|   |   |   |   +---Agents of S.H.I.E.L.D. S2
|   |   |   |   +---Agents of S.H.I.E.L.D. S3
|   |   |   |   +---Agents of S.H.I.E.L.D. S4
|   |   |   |   +---Agents of S.H.I.E.L.D. S5
|   |   |   |   +---Agents of S.H.I.E.L.D. S6
|   |   |   |   \---Agents of S.H.I.E.L.D. S7
|   |   |   +---Ant-Man (2015,2018,2023)
|   |   |   +---Avengers, The (2012,2015,2018,2019)
|   |   |   +---Black Panther (2018,2022)
|   |   |   +---Black Widow (2021)
|   |   |   +---Captain America (2011,2014,2016)
|   |   |   +---Captain Marvel (2019,2023)
|   |   |   +---Deadpool & Wolverine (2024)
|   |   |   +---Doctor Strange (2016,2022)
|   |   |   +---Echo (2024) [Series]
|   |   |   +---Eternals (2021)
|   |   |   +---Falcon and the Winter Soldier, The (2021) [Series]
|   |   |   +---Guardians of the Galaxy (2014,2017,2023)
|   |   |   +---Hawkeye (2021) [Series]
|   |   |   +---I Am Groot (2022,2023) [Series]
|   |   |   +---Incredible Hulk, The (2008)
|   |   |   +---Iron Man (2008,2010,2013)
|   |   |   +---Loki (2021,2023) [Series]
|   |   |   +---Marvel One-Shot (2011,2011,2012,2013,2014)
|   |   |   +---Marvel Studios - Legends (2021) [Series]
|   |   |   +---Moon Knight (2022) [Series]
|   |   |   +---Ms. Marvel (2022) [Series]
|   |   |   +---Secret Invasion (2023) [Series]
|   |   |   +---Shang-Chi and the Legend of the Ten Rings (2021)
|   |   |   +---She-Hulk - Attorney at Law (2022) [Series]
|   |   |   +---Spider-Man (2017,2019,2021)
|   |   |   +---Thor (2011,2013,2017,2022)
|   |   |   +---WandaVision (2021) [Series]
|   |   |   \---What If... (2021,2023) [Series]
|   |   \---[Sony's Spider-Man Universe]
|   |       +---Kraven the Hunter (2024)
|   |       +---Madame Web (2024)
|   |       +---Morbius (2022)
|   |       \---Venom (2018,2021,2024)
|   \---[Others]
|       +---Aeon Flux (2005)
|       +---Alita - Battle Angel (2019)
|       +---Bloodshot (2020)
|       +---Dora and the Lost City of Gold (2019)
|       +---Dragonball Evolution (2009)
|       +---Ghost in the Shell [Animation�Movie]
|       |   +---Ghost in the Shell (2017) [Movie]
|       |   \---Koukaku Kidoutai - SAC_2045 (2020,2022) [Animation]
|       +---Knights of the Zodiac (2023)
|       +---Looney Tunes - Back in Action (2003)
|       +---One Piece (2023) [Series]
|       +---Scooby-Doo (2002,2004)
|       +---Space Jam (1996,2021)
|       +---Teenage Mutant Ninja Turtles (2014,2016)
|       +---Tom and Jerry (2021)
|       \---Transformers (2007,2009,2011,2014,2017,2018,2023)
\---[Tokusatsu]
    +---Cutie Honey - The Live (2007)
    +---Garo
    |   +---1. Garo (2005)
    |   +---2. Garo - Makai Senki (2011)
    |   +---3-5 Zero - Black Blood (2014)
    |   +---3. Garo - Yami o Terasu Mono (2013)
    |   +---4. Garo - Makai no Hana (2014)
    |   +---5. Garo - Gold Storm Sho (2015)
    |   +---6-5 Zero - Dragon Blood (2017)
    |   +---6. Garo - Makai Retsuden (2016)
    |   +---7. Kami no Kiba - JINGA (2018)
    |   +---8. Garo - Versus Road (2020)
    |   \---9. Garo - Hagane wo Tsugu Mono (2024)
    +---Kamen Rider
    |   +---Kamen Rider (Heisei 1)
    |   |   +---Kamen Rider 555 (2003)
    |   |   |   \---Kamen Rider 555 [Spc]
    |   |   +---Kamen Rider Agito (2001)
    |   |   +---Kamen Rider Blade (2004)
    |   |   +---Kamen Rider Decade (2009)
    |   |   |   +---Kamen Rider Decade 29-5 [Spc] Protect! The World of Televikun
    |   |   |   \---Kamen Rider Decade [Spc]
    |   |   +---Kamen Rider Den-O (2007)
    |   |   |   \---Cho Kamen Rider Den-O
    |   |   +---Kamen Rider Hibiki (2005)
    |   |   |   \---Kamen Rider Hibiki [Spc]
    |   |   +---Kamen Rider Kabuto (2006)
    |   |   +---Kamen Rider Kiva (2008)
    |   |   |   \---Kamen Rider Kiva [Spc]
    |   |   +---Kamen Rider Kuuga (2000)
    |   |   \---Kamen Rider Ryuki (2002)
    |   +---Kamen Rider (Heisei 2)
    |   |   +---Kamen Rider Build (2017)
    |   |   |   +---Kamen Rider Build - NEW WORLD
    |   |   |   +---Kamen Rider Build 21-5 [Spc] Raising the Hazard Level ~7 Best Matches~
    |   |   |   \---Kamen Rider Build [Spc]
    |   |   +---Kamen Rider Drive (2014)
    |   |   |   +---Kamen Rider Drive - Saga
    |   |   |   \---Kamen Rider Drive [Spc]
    |   |   +---Kamen Rider Ex-Aid (2016)
    |   |   |   +---Kamen Rider Ex-Aid - Another Ending
    |   |   |   \---Kamen Rider Ex-Aid [Spc]
    |   |   +---Kamen Rider Fourze (2011)
    |   |   |   \---Kamen Rider Fourze 38-8 [Spc] Rocket Drill States of Friendship
    |   |   +---Kamen Rider Gaim (2013)
    |   |   |   +---Kamen Rider Gaim - Gaiden
    |   |   |   \---Kamen Rider Gaim 11-5 [Spc] Fresh Orange Arms is Born!
    |   |   +---Kamen Rider Ghost (2015)
    |   |   |   +---Kamen Rider Ghost 12-5 [Spc] Ikkyu
    |   |   |   +---Kamen Rider Ghost 20-5 [Spc] Legendary! Riders' Souls!
    |   |   |   \---Kamen Rider Ghost [Spc]
    |   |   +---Kamen Rider OOO (2010)
    |   |   |   \---Kamen Rider OOO 36-5 [Spc] Quiz, Dance, and Takagarooba!
    |   |   +---Kamen Rider W (2009)
    |   |   |   +---Fuuto Tantei (2022) [Anime]
    |   |   |   \---Kamen Rider W - Returns
    |   |   +---Kamen Rider Wizard (2012)
    |   |   |   \---Kamen Rider Wizard [Spc]
    |   |   \---Kamen Rider Zi-O (2018)
    |   |       +---Kamen Rider Zi-O - Next Time
    |   |       +---Kamen Rider Zi-O - Rider Time
    |   |       \---Kamen Rider Zi-O [Spc]
    |   +---Kamen Rider (Other)
    |   |   +---Kamen Rider (Movie)
    |   |   +---Kamen Rider Amazons (2016,2017)
    |   |   \---Kamen Rider Black Sun (2022)
    |   \---Kamen Rider (Reiwa)
    |       +---Kamen Rider Gavv (2024)
    |       +---Kamen Rider Geats (2022)
    |       |   +---Kamen Rider Geats - Extra
    |       |   \---Kamen Rider Geats [Spc]
    |       +---Kamen Rider Gotchard (2023)
    |       |   \---Kamen Rider Gotchard [Spc]
    |       +---Kamen Rider Revice (2021)
    |       |   +---Kamen Rider Revice 16-5 [Spc] The Mystery
    |       |   \---Kamen Rider Revice [Spc]
    |       +---Kamen Rider Saber (2020)
    |       |   \---Kamen Rider Saber [Spc]
    |       \---Kamen Rider Zero-One (2019)
    |           +---Kamen Rider Zero-One - Kamen Rider Genms
    |           +---Kamen Rider Zero-One - Others
    |           \---Kamen Rider Zero-One [Spc]
    +---Pretty Guardian Sailor Moon (2003)
    +---Shougeki Gouraigan (2013)
    +---Tokusatsu GAGAGA (2019)
    \---Unofficial Sentai Akibaranger (2012,2013)
