Folder PATH listing for volume Master
Volume serial number is CA6B-31D2
E:.
+---[Disney]
|   +---A Bug's Life (1998)
|   +---Atlantis (2001,2003)
|   +---Baymax! (2022) [Series]
|   +---Big Hero 6 (2014)
|   +---Bolt (2008)
|   +---Brave (2012)
|   +---Cars (2006,2011,2017)
|   +---Cars on the Road (2022) [Series]
|   +---Chicken Little (2005)
|   +---Chip 'n Dale - Rescue Rangers (2022)
|   +---Encanto (2021)
|   +---Enchanted (2007,2022)
|   +---Finding Nemo (2003,2016)
|   +---Frozen (2013,2019)
|   +---Good Dinosaur, The (2015)
|   +---Incredibles, The (2004,2018)
|   +---Inside Out (2015)
|   +---Lightyear (2022)
|   +---Lilo & Stitch (2002,2005)
|   +---Luca (2021)
|   +---Mickey and the Roadster Racers (2017,2018.2019)
|   |   +---Mickey and the Roadster Racers S1
|   |   +---Mickey and the Roadster Racers S2
|   |   \---Mickey and the Roadster Racers S3
|   +---Mickey Mouse (1999,2004)
|   +---Moana (2016)
|   +---Mulan (1998,2004)
|   +---Onward (2020)
|   +---Planes (2013,2014)
|   +---Ralph (2012,2018)
|   +---Ratatouille (2007)
|   +---Raya and the Last Dragon (2021)
|   +---Ron's Gone Wrong (2021)
|   +---Soul (2020)
|   +---Spies in Disguise (2019)
|   +---Strange World (2022)
|   +---Tangled (2010)
|   +---Tinker Bell (2008,2009,2010,2012,2014,2015)
|   +---Toy Story (1995,1999,2010,2019)
|   +---Treasure Planet (2002)
|   +---Turning Red (2022)
|   +---Up (2009)
|   \---WALL-E (2008)
+---[Marvel]
|   +---I Am Groot (2022) [Series]
|   +---Spider-Man - Into the Spider-Verse (2018)
|   \---What If... (2021) [Series]
+---[Others]
|   +---Detective Conan (1996) [Anime]
|   |   +---Detective Conan - Zero's Tea Time (2022) [Anime]
|   |   +---Detective Conan [BO]
|   |   +---Detective Conan [Mov]
|   |   \---Detective Conan [OVA�Spc]
|   +---Digimon [Anime]
|   |   \---Digimon Universe - Appli Monsters (2016) [Anime]
|   +---Dragon Ball Kai (2009,2014) [Anime]
|   |   +---Dragon Ball Kai 2009 S1
|   |   +---Dragon Ball Kai 2009 S2
|   |   +---Dragon Ball Kai 2009 S3
|   |   +---Dragon Ball Kai 2009 S4
|   |   +---Dragon Ball Kai 2014 S5
|   |   +---Dragon Ball Kai 2014 S6
|   |   \---Dragon Ball Kai 2014 S7
|   +---Element Hunters (2009) [Anime]
|   +---Hataraku Saibou [Anime]
|   |   +---Hataraku Saibou (2018,2021) [Anime]
|   |   \---Hataraku Saibou Black (2021) [Anime]
|   +---MegaMan [Anime]
|   |   +---MegaMan NT Warrior (2002,2002) [Anime]
|   |   +---MegaMan NT Warrior - Axess (2003) [Anime]
|   |   +---MegaMan NT Warrior - Beast (2005) [Anime]
|   |   +---MegaMan NT Warrior - Beast+ (2006) [Anime]
|   |   +---MegaMan NT Warrior - Stream (2004) [Anime]
|   |   \---MegaMan NT Warrior - The Program of Light and Darkness (2005) [Anime] [Mov]
|   +---Phi Brain - Kami no Puzzle (2011,2012,2013) [Anime]
|   \---Yu-Gi-Oh! [Anime]
|       +---1. Yu-Gi-Oh! (1998) [Anime]
|       +---2. Yu-Gi-Oh! Duel Monsters (2000) [Anime]
|       |   +---Yu-Gi-Oh! Duel Monsters 001-049
|       |   +---Yu-Gi-Oh! Duel Monsters 050-097
|       |   +---Yu-Gi-Oh! Duel Monsters 098-144
|       |   +---Yu-Gi-Oh! Duel Monsters 145-184
|       |   \---Yu-Gi-Oh! Duel Monsters 185-224
|       \---Yu-Gi-Oh! [Mov]
\---[Preschool]
    +---Bernard Bear (2006)
    +---CoComelon (2020)
    +---D Billions (2019)
    +---[Kid Songs]
    +---[kidzooly.fivelittle]
    +---[Series]
    |   +---Jimmy Neutron
    |   |   +---Adventures of Jimmy Neutron - Boy Genius, The (2002,2003,2004)
    |   |   |   +---The Adventures of Jimmy Neutron - Boy Genius S1
    |   |   |   +---The Adventures of Jimmy Neutron - Boy Genius S2
    |   |   |   \---The Adventures of Jimmy Neutron - Boy Genius S3
    |   |   +---Jimmy Neutron - Boy Genius (2001)
    |   |   \---Jimmy Timmy Power Hour, The (2004,2006,2006)
    |   +---Pororo - the Little Penguin (2003)
    |   |   +---Eddy - The Clever Fox S1
    |   |   +---Eddy - The Clever Fox S2
    |   |   +---Loopy - The Cooking Princess S1
    |   |   +---Pororo - the Little Penguin S1
    |   |   +---Pororo - the Little Penguin S2
    |   |   +---Pororo - the Little Penguin S3
    |   |   +---Pororo - the Little Penguin S4
    |   |   +---Pororo - the Little Penguin S5
    |   |   \---Pororo [Mov�Spc]
    |   +---Robocar Poli (2011)
    |   |   +---Robocar Poli S1
    |   |   +---Robocar Poli S2
    |   |   +---Traffic Safety with Poli S1
    |   |   \---Traffic Safety with Poli S2
    |   +---Super Wings (2015)
    |   |   +---Super Wings S1
    |   |   \---Super Wings S2
    |   +---Tayo, the Little Bus (2010)
    |   +---Tobot (2010)
    |   |   +---Tobot Athlon S1
    |   |   +---Tobot Athlon S2
    |   |   +---Tobot Athlon S3
    |   |   +---Tobot S1
    |   |   +---Tobot S2
    |   |   +---Tobot S3
    |   |   \---Tobot S4
    |   +---Upin & Ipin (2007)
    |   |   +---Upin & Ipin S1
    |   |   +---Upin & Ipin S2
    |   |   +---Upin & Ipin S3
    |   |   +---Upin & Ipin S4
    |   |   +---Upin & Ipin S5
    |   |   +---Upin & Ipin S6
    |   |   +---Upin & Ipin S7
    |   |   \---Upin & Ipin S8
    |   \---[Top Choice]
    |       +---Pororo - the Little Penguin (2003)
    |       +---Robocar Poli (2011)
    |       +---Super Wings (2015)
    |       \---Upin & Ipin (2007)
    \---[Sunday School Songs]
        \---74. LAGU ANAK SEKOLAH MINGGU
            \---[Eliminated]
