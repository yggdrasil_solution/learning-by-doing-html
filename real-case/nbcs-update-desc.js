function onBodyLoad() {
	onElementChangeUpdateDesc('xCode', 'xDesc');
}

function onElementChangeUpdateDesc(elementCode, elementDscp) {
	var element = document.getElementById(elementCode);
	var label = element.options[element.selectedIndex].innerHTML;
	var dscp = (label.split("-")[0] + "").replace(" ", "");
	document.getElementById(elementDscp).value = dscp;
}